# Term

Term is a Discord bot that will let you bash run commands in Docker containers. In other words, you can have a totally isolated Ubuntu container per channel.

## Usage

Here are some of the commands you can use, and what they do:

- `$ info` will show some info about Term, and also list the commands
- `$ ping` will respond with "pong"
- `$ create` will create a container for the current channel if one doesn't exist
- `$ remove` will remove the current channel's container if it exists
- `$ <command>` or `$ run <command>` will run bash commands in the current channel's container

## Running

Since Term is open-source you might want to run it yourself, for whatever reason. Here's how.

### Installation

You'll need to [install Docker](https://docs.docker.com/install/) first.

```
$ git clone https://gitlab.com/pwnsquad/term.git
$ npm install
$ docker pull ubuntu
```

### Running

Term uses the environment variable `BOT_TOKEN` for your bot token. Make sure to create a bot on Discord's developer dashboard.

Here's how to run it normally:

```
$ BOT_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxx" npm start
```

We recommend running it with PM2 if you're going to be using Term for environments where you don't want the bot to fail and you want better resource management. To do that, you only have to install PM2, then edit `ecosystem.config.js` to include your bot's token.

```
$ npm install -g pm2
$ nano ecosystem.config.js
$ pm2 start
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Development

While working on Term's code you might want the bot to automatically restart when you make changes. Here's how:

```
$ BOT_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxx" npm run dev
```